
# Hello Angular2

## Set Up

### Install
`
npm install
`

### Build
`npm run build`

### Clean
`npm run clean`

### Run Typescript Compiler
`npm run tsc`

### Run Webpack
`npm run webpack`

### Use the typescript compiler
`
./node_modules/.bin/tsc 
`

### Use webpack
`
./node_modules/.bin/webpack
`

## Reference
` 
https://semaphoreci.com/community/tutorials/setting-up-angular-2-with-webpack
`

`
https://github.com/AngularClass/angular2-webpack-starter
`

`
https://github.com/schempy/angular2-typescript-webpack
http://schempy.com/2016/01/19/angular2_webpack_typescript/
`

`
http://blog.scottlogic.com/2015/12/07/angular-2.html
`

`
https://github.com/mgechev/angular-seed
`

`
https://www.barbarianmeetscoding.com/blog/2016/03/25/getting-started-with-angular-2-step-by-step-1-your-first-component/
`
`
http://wouterdekort.com/2016/04/04/typings-for-typescript/
`