import { Component } from '@angular/core';

@Component({
  selector: 'hello-angular2',
  template: 'Hello Angular2',
})
export class AppComponent {
  constructor() {
    console.log('Hello Angular2');
  }
}